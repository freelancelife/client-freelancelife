import React from 'react';

const Task = ({title, onClick}) => {
  return <li
    className="list-group-item"
    onClick={onClick}
    >{title}</li>
}

export default Task;
