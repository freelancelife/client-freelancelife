import React, { Component } from 'react';

class AddTask extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false,
      title: "", 
      type: "item",
      isLoading: false,
      isError: false,
      didInvalidate: true,
      items: []
    }

    this.onShow = this.onShow.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.save = this.save.bind(this);
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  onShow() {
    this.setState({show: !this.state.show});
  }

  save() {
    this.setState({isLoading: true})

    let body = {
      title: this.state.title,
      type: this.state.type,
      parent: this.props.parentTasks || null
    };

    let hostname = window.location.hostname;
    let url = 'http://'+ hostname +':5000/tasks';

    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/x-www-form-urlencoded',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      mode: 'cors',
      body: 'set='+ JSON.stringify(body)
    })
    .then(res => res.json())
    .then(data => {
      console.log('this.props: ', this.props);
      this.props.invalidate();
      this.setState({
        isLoading: false,
        show: false,
        title: "", 
        type: "item",
        isError: false
      });
    })
    .catch(err => {
      this.setState({isLoading: false, isError: true});
      console.log(err);
    });

    console.log(body);
  }

  render() {
    if(!this.state.show) {
      return (
        <li onClick={this.onShow} className="list-group-item">
          + Добавить ещё одну карточку
        </li>
      )
    }

    return (
    <li className="input-group">
      <input value={this.state.title} type="text" name="title" className="form-control" placeholder="Введите название карточки" aria-label="Recipient's username with two button addons" aria-describedby="tasks title" onChange={this.handleChange}/>
    <select name="type" value={this.state.type} onChange={this.handleChange} className="custom-select" aria-label="Example select with button addon" style={{"maxWidth": "121px"}}>
        <option value="item">Эллемент</option>
        <option value="group">Группа</option>
      </select>
      <div className="input-group-append" id="button-addon4">
        <button className="btn btn-outline-success" type="button" onClick={this.save}>Добавить</button>
        <button onClick={this.onShow} className="btn btn-outline-danger" type="button">Отмена</button>
      </div>
    </li>
    )
  }
}

export default AddTask;
