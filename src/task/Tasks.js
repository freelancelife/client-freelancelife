import React, { Component } from 'react';
import Task from './Task';
import AddTask from './AddTask.js';
import TaskGroup from './TaskGroup';

class Tasks extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      isError: false,
      didInvalidate: true,
      items: []
    }

    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    this.setState({isLoading: true})
    let hostname = window.location.hostname;
    let parentTasks = (this.props.parentTasks) ?
      '/' + this.props.parentTasks : '';

    let url = 'http://'+ hostname +':5000/tasks' + parentTasks;

    fetch(url)
    .then(res => res.json())
    .then(data => this.setState({
      items: data.items, isLoading: false
    }))
    .catch(err => {
      this.setState({isLoading: false, isError: true});
      console.log(err);
    });
  }

  render() {
    if(this.state.isLoading) {
      return (
        <ul className="list-group">
          <li className="list-group-item">Loading...</li>
        </ul>
      );
    }

    var items = this.state.items.map(task => {
      if(task.type === 'group') {

        return <TaskGroup key={task.id} {...task}/>
      }

      return <Task key={task.id} {...task}/>
      
    });

    return (
      <ul className="list-group">
        {items}
        <AddTask invalidate={this.getData} parentTasks={this.props.parentTasks}/>
      </ul>
    )
  }
}

export default Tasks;
