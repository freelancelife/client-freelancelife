const api = require('./api');
const Tasks = require('./Tasks').default;

module.exports = { 
  api,
  Tasks
}
