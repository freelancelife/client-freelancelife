import React, { Component } from 'react';
import Task from './Task';
import Tasks from './TaskGroup';


class TaskGroup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false
    }

    this.show = this.show.bind(this);
  }

  show() {
    this.setState({show: !this.state.show});
  }
   
  render() {
    const {id, title} = this.props;

    if(this.state.show) {
      return [
        <Task onClick={this.show} id={id} title={title}/>,
        <Tasks parentTasks={id} />
      ];
    }

    return ( 
      <Task onClick={this.show} id={id} title={title}/>
    )

  }
}

export default TaskGroup; 
